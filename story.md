# Kit

## 1

Kit Andebert was always good with his fists, possibly a little too good. When he still only stood as tall as his father's knee, he permanently separated another Tabaxi boy from three of his front teeth for snickering next to him. It didn't matter if the boy was actually laughing at him or not, but it *sounded* like he was and Kit was having a bad day. In his teenage years, he had friends---good friends---but even they would keep their distance if it looked like Kit was having a bad day. The last to ask what was wrong was left behind with an aching gut and all the wind knocked out of his lungs. Kit apologized later, even thanked his friend, but the next time Kit seemed to be in a sour mood, that particular friend was nowhere to be found.

By the time young adulthood came to Kit, he had considerably mellowed out. He became more able to control his temper, largely due to meeting a female Tabaxi named Margaret Sein. He loved her more than anyone he had ever met, and just being around her seemed to mellow him out. If something sparked that previously uncontrollable red he often saw in his younger years, all it took was a thought of Margaret's sleek black fur and pretty green eyes, always full of love for him, to make the red melt away. He'd unclench his fists, shake his head, and walk away---something his parents had tried to teach him to do since he was a boy.

Unfortunately, at around the same time he met Margaret, he met what he soon found to be the other love of his life. Drinking. It started as a night out a week with his friends, sitting in the bars, tucking away a few drinks while chatting. Then it was two nights a week. Then three. Friends started to taper off---sometimes showing up only once or twice again, others not at all as Kit started drinking more and more. When Kit drank---which, within a year of starting these bar crawls he did more in one night than he didn't and more per night than he used to do in a month---he got mean. As mean as he ever was before. Maybe worse. When he drank, the truth was, the thought of Margaret never even came to him. And when he did think of Margaret, it was of her admonishing expression as he came home bruised and swollen from his most recent drunken bar fight.

But he never hit her. He hated her expression, but it wasn't because it made him angry. It was the guilt that expression sparked in him because Margaret always had a way of making him see things rationally, and in that moment when he'd stumble in drunk and fall subject to her gaze, he knew he'd messed up again. And he knew she was right.

One night, they had a talk. He agreed to stop drinking. It wouldn't be easy, but he'd do it for her. No, she'd corrected him that night, he'd do it for himself. Both of them. He was right about the not being easy part. On many rough nights, he sat outside his old haunt fighting the voice that said just one drink would make him feel better---it was especially bad on the nights his friends would be there---but every time that voice whispered in his ear, Margaret's came to him stronger, reassuring. He went home sober. It got easier over time, and eventually things got better.

For a while.

Kit fell off the wagon on the anniversery of what he began to think of as The Intervention. By then he hardly thought about drinking or fighting; he could pass the bar without even a glance as he went by. Later, he would think, *that's what made it happen, I was too confident I was in control.* At the time it happened, he never had such a thought.  

At the time it happened, he was working the register at the community's general store. Margaret's uncle owned the place, and she had gotten him the job when he had finally sobered up. He didn't mind the job, usually, but maintaining a professional demeanor in the face of frustrating customers was still foreign to him. Sometimes, that old red rage still boiled somewhere far down and he had to push it further down still since he couldn't walk away. Not at the store.

Two weeks into the job, Marlow Trelby, who Kit knew only as the Tabaxi with weird teeth (Kit had no memory of knocking the man's teeth out as a kid), walked into the store. He had gotten fat, and when he smiled it was a leering thing that made his jowls jiggle like they were filled with melted butter. Kit had the idea they probably were. Marlow favored Kit with one such smile as he walked up to the register today.

"Well I'll be damned," the fat Tabaxi said, smiling wider, getting almost in Kit's face across the counter. "Kit Andebert! It's been a while. Tell me, how did the angry town drunk get a job up front at the register?"

Kit swallowed that one. He had gotten good at swallowing things---though they usually weren't personal attacks. Marlow had a grudge. More than that, Kit realized, he *knew* Kit couldn't do anything behind the register. He'd lose his job and that would be that.

"I've been sober for a year," Kit said simply. 

"You're girl got you the position, didn't she?" Marlow said, his smile stretching impossibly wider. "Say, how'd you end up with *her* anyway? You're just an angry drunk. I'm just amazed she'd put up with you." He shrugged.

And Kit swallowed that too, although now his fists were curled beneath the counter. He was seriously weighing whether it was worth losing his job to turn his lard's face into hamburger.

"She deserves better, you know."

"Are you going to buy anything?" Kit asked, his voice tight but still just barely under control.

"From you? No thanks," Marlow said. "Let me know when you break up. I want a turn." He turned on his heel---somehow both clumsy and arrogant at once---and strode out of the store. Waddled, Kit thought bitterly. That shit *waddled* out. It didn't make him feel better.

And he didn't feel better by the end of his shift. If anything, he felt worse now that the conversation had a chance to replay itself over and over in his head. He needed a drink. It had been a year, hadn't it? Just one would be okay. Yeah, he'd stop at one and go home and talk it over with Margaret. Two at most.

He finally stopped---got cut off, would be a better phrase---at thirteen and stumbled home alternating between singing old campfire songs off-key and cursing out a fat Tabaxi who was nowhere to be seen this evening. The first person he did see after leaving the bar was Margaret.

"You're drunk." It was the first thing she said to him. Between the front door and the kitchen there had been nothing but silence. In the kitchen, she had turned to him, her expression dark---angry? sad?---and said those two words. It was *all* she said. She was looking down on him. Marlow's words came back to him: *She deserves better, you know.* It struck him all at once. She thought she was better than him? Was that why she stayed with him? Because she liked feeling better than someone? In his drunken stupor, it made perfect sense. He saw red.

It was almost like a fog came over his vision. Thick, red clouds billowing across his thoughts, crazily in front of his eyes, dancing back and forth to a backdrop of yelling voices, words, "stop! Kit, stop!" Clatters. Table legs scraping. Sobbing.

The fog cleared. Kit's fists hurt, and he could see why---thought he was having a bad dream at first, a very bad dream, but it wasn't. Somewhere, he knew that. The kitchen table had been slammed up against the wall. Plates had fallen and lay shattered on the floor. Margaret sat among them, slumped against a cracked leg of the kitchen table holding her face. Drops of dark red glistened against her dark fur. It felt like hours passed in that kitchen---horribly quiet hours---before she looked up. Her eye was already beginning to swell under her blood-matted fur. Her lower lip and eyebrow were both split open, gashes that looked almost accusatory on her wounded face. Tears were welling up in her eyes, but worse than that was the surprise. And the hurt. She moved her lips, but all Kit could hear was a thin whisper. He shifted closer, suddenly terrified of what he would hear. She said it again. Two words---just like when he came in, Kit thought and almost laughed at the absurdity of it, but there was nothing to laugh about. The two words were simple but clear: "Get out."

## 2

He stumbled his way to the park and walked around for a while. At some point it had gotten late, and he was here by himself. He sat on one of the wooden benches lining the walkways, thought about how he and Maragaret used to sit on these benches a lot. Thought about how he had just ended all of that. He'd kill Marlow if he saw that fat bastard again. This was his fault, at least in part. A *big* part. But who was the idiot who fell for his goading and went into a drunken rage? Kit growled to himself, stood, and turned to the tall oak standing beside the benching. He threw his fist at it, imagining Marlow's face. In his mind, Marlow's nose bent inward with a sickening snap, folding into that pudgy face of his. In reality, the snapping was the sound of two of Kit's fingers breaking against the wood. He barely felt it. Despite the impending headache, his hands were still mostly numb from the booze.

"Well, someone's having a bad day," a voice said from beside him. Kit wasn't sure when the human got there. The man looked to be in his mid-fifties and smiled as if he was passing a buddy in the street, not an angry, drunk Tabaxi. "Mind if I sit?"

"Piss off, old man," Kit said. The urge to slug the guy in the face rose up in him like a sneak attack; now that he broke the seal, it seemed there was no stopping it. But before he could make a move, the older man surprised him. He rushed forward and slugged Kit right in the gut. Kit doubled over, the wind knocked out of him. Was he being robbed? No, the oldie wasn't following up. He was just waiting for Kit to regain his bearings, and that pissed Kit off even more. 

Kit's lungs finally refilled with air, and he lunged at the old man with his fists clenched---broken fingers and all. He went at him meaning to do what he had imagined doing to Marlow, but the old man pushed Kit's hand aside in such a smooth gesture Kit wasn't sure what happened. His fist was suddenly going in the wrong direction, and then there was a pressure on his back and he went to the ground, breathing hard.

"You really did a number on that woman," the man said, standing over Kit. He wasn't smiling anymore.

"How---?"

"I was passing by when you stormed out. Had a bad feeling by the way you left, so I went to check. I performed some basic healing and got her help, if you care."

"I do." Kit said, the anger gone out of him. After a moment of silence, he clumsily tacked on, "Thanks."

The man said nothing in response, then carefully sat down on the bench, still looking down at Kit. "I don't sense that you're a bad person, Tabaxi. Despite what you did to your wife. And you're a pretty good fighter, too. But you need control." He pulled out a pipe and a match, lit the pipe, and took a long drag from it. "You can't take back what you did tonight---you let your anger control you this time and it's done---but control can be learned, and you can always improve yourself."

Kit said nothing, unsure where the man was going with this, opting to nod instead.

"Come with me," the man said, standing. "Come live in my cloister for a year, and we will teach you control so such nights as tonight never have to happen again."

For a couple seconds, Kit remained silent. A cloister? This man wanted Kit to become a *monk*? There was no way. A laugh worked its way into Kit's throat, but died halfway to his mouth as reality crashed in alongside the recollection of two words: "Get out." He could fight Margaret over the house, sure, but it wouldn't really be home. Not anymore. And his job? That had been given to him through Margaret, too, and he didn't think it would be there waiting for him in the morning. The couple friends he had left, he didn't see too often these days. Most of their socializing was of the drinking at the Rusty Claw variety, and tonight was evidence of where that would leave Kit. With nothing. Absolutely nothing. Not here, anyway.

"Okay," Kit said, less an answer than him trying to convince himself. He pushed himself off the ground with his good hand. "Okay." Firmer this time, more sure.

They walked most of the night, stopping once to splint Kit's broken fingers and a second time to refill their canteens (Kit's having been lent by the monk, who's name turned out to be Ranulf Nordbeck). The cloister came into view just before dawn, and by the time the sun was barely in the sky, Kit was being introduced to his new dwelling.

"Rest," Nordbeck said. "Today you sleep off the walk and the booze. Tomorrow, we work you raw. Understand?"

And work him raw they did. Later, Kit would describe the first few weeks at the cloister as a sort of hell on earth, but as the monk had promised, he had honed his fighting skills. And, more importantly, he had learned control. By the time his year at the cloister was up, Kit had come to think of this as his life. He never drank, and thoughts of that old life where he did were few and far between. The only thoughts he had of his old life were thoughts of Margaret. He often wondered how she was doing, if she had met someone new, someone better than him, since he had left. Part of him hoped so. It would be good for her. And another part of him dreaded it. Regardless of the answer, he wanted---no, needed---to go back and apologize to her, bow before her with his head in the dirt if it came to that. He didn't need her forgiveness---the monks had taught him well that doing things expecting some sort of return was simply not the way---but she deserved the apology.

But he dreaded that moment too. If he was being honest with himself, which he almost always was these days, he was scared to see her. Scared to see that she hated him. He stayed at the cloister for three more years.

The thing that changed that was a note that showed up one day. His name---Kit Andebert---was scrawled on the front of the envelope in handwriting that, even after four years, he hadn't forgotten. It was a note from Margaret.

> Dear Kit,
>
> I hope this letter finds you well. I've missed you, you know. After that night four years ago, you just vanished. For a while, I worried that something might have happened to you in the night. I thought, perhaps, my words forced you to seek shelter in the wilderness and you were taken by one of the beasts out there. Therefore, it is a great relief to learn you have been taking dinner with the monks to the north. I hope it's been good for you!
>
> Please don't misunderstand. I haven't forgiven you for what you did, but I want to see you again. I've heard you've been doing a lot of good with the monks up there. Saving those children from that fire must have been terrifying! That is to say, I think I could forgive you, if only you would visit. I hope you will soon, or at the very least write back.
>
> I am proud of how far you've come.
>
> With Love,
>
> Margaret Sein

Kit put down the letter and wiped the moisture from the fur beneath his eyes. She still loved him after all. That was good. That was really good. He read the letter seven more times before putting it down, certain that he would find he'd misread it, but he hadn't.

A month later, after finishing his business around the cloister and making the rounds to say goodbye to what he thought of now as his family (many promises were made about future visits), he set out on his journey back to see Margaret.

## 3

The trip back was half a day's walk, made long by the constant barage of thoughts filling Kit's head. How was Margaret doing? How would she look? What about his old drinking buddies? Was the Rusty Claw still around? A lot could have changed in the past four years, and the closer Kit got, the more anxious to see his old home he became. When the first thatch roof of his hometown came into view, he broke into a trot (was the old man that lived there still alive?); when the windows rose over horizon, his trot became something closer to a run. Trees passed by on either side and fell away as he approached the town. Above him, the sun was shining and puffy white clouds drifted in the warm breeze. It was the nice, quiet sort of day on which he used to enjoy taking walks with Margaret. 

It really was quiet.

He stopped. At some point between the woods and entering the town, the quiet had snuck up around him. A sickening tingle seemed to drop down his throat and settle heavily into his stomach. This was wrong. There should have been noises---kids playing in the street, merchants calling out to bustling crowds, horse hooves clacking against the ground as they dragged wagons along behind them.

Kit only heard his own footsteps.

He walked farther, his unease turning to something like fear as he reached the town square with the big fountain in the middle and still saw no one. On a day like today, he had never seen this place *not* bustling, let alone abandoned. His eye caught on something near the base of the fountain, and he bent down to get a close look. It was a play-battlefield. He could see the image in his head easily: a group of kids playing by the fountain as their mommies and daddies went shopping around the square. They would have their straw dolls with their tiny swords set up just like this, in a battlefield formation. The one on its back had been struck first and went down, the child whose doll it was pouting at being out. The two that were standing (leaning against each other was more like it) were actually, in the minds of the kids, pitted in pitched combat. The outcome could go either way, depending on how imaginative the kids were. Their parents would come back and tell them it was time to go, and they would beg for five more minutes, which they would be granted. Then they would have left, taking their toys with them to battle another day.

Except they hadn't. Taken the toys with them, that was. 

The tingle in the pit of Kit's stomach seemed to gain weight, forming into a tight, heavy ball. Something had happened here, but what? If there was an emergency, would the dolls have been left in perfect formation? No, the kids would've tried to take them along, but this battlefield looked like everyone had just vanished around it, leaving it undisturbed. Now that the thought occurred to him, there were other things too---a whole fish on the counter of a food cart (as if a housewife had been just about to pay for it), an almost-finished game of chess set up at one of the tables around the square, an open book beside a cup of coffee sitting at another (Kit lifted the coffee cup experimentally and found it cool to the touch; it had been left a while ago). If something had happened, like an attack on the town, everything would have been in complete disarray... but nothing was. It just like everyone just got up and left.

Margaret.

He forgot about the dolls, the fish, the cold coffee. Where was Margaret? He took off again, this time at a full run---he had gained a lot of stamina during the past four years---towards his old house, already knowing what he was going to find. But he refused to believe it until he saw it for himself. The house wasn't far from the town square (he passed the Rusty Claw along the way but paid it no mind, didn't even notice it), and he was in the yard staring at the door within ten minutes.

He slowed down and came to a stop at the door, suddenly wishing he was anywhere but here. Whatever he found inside, he wasn't going to like it; of that, he was sure. He reached for the handle then paused as an absurd thought came to him: *should I knock?* It seemed ridiculous to knock at his own front door, but it wasn't really his anymore, was it? It had stopped being his four years ago. He swallowed down his fear, not liking the damp, sweaty feel under his fur, and knocked.

A minute later, he knocked again.

Five minutes later, he knocked a third time. Louder.

No one came. He had known no one would come the first time he knocked, but he held on to some crazy hope that maybe Margaret was still here, that whatever happened hear had given her a miss. *Maybe she's just out?* his mind insisted, then countered itself with, *Out where? The whole town has vanished.* He tried the doorknob and found it turned easily; the house had been left unlocked, which Margaret only did when she was actually home. The door swung back, and he stepped into the vestibule.

"Margaret?" he called, and in the silence, his own voice startled him. There was no reply. He stepped further into the house. Not much changed since he had left. The living room had the same furniture as when he'd stumbled in here drunk for the last time. The hallway runner was the same brown, diamond pattern he remembered. The only thing that seemed different was the new table in the kitchen. Except, in a way, it *all* looked different, but Kit couldn't place exactly why. Everything was the same---the furniture hadn't even been moved. But still...

He headed to the staircase and called up the stairs, "Marg?" Nothing. He walked up and searched the bedrooms. No Margaret, but there was a basket of laundry sitting on their bed *(just hers now)*. He thought of the straw soldier dolls, the fish, the chess game. Margaret had never brought the laundry up and just stopped doing it, especially if it had just finished drying; she swore it would get wrinkled if not folded prompty. Yet here it was, a basket of clean laundry abandoned like everything else. That was the thing that convinced him.

He sat on the bed (same bedspread, but somehow different like everything else), lowered his face into his hands, and shivered violently. This was wrong. This was all wrong. After a moment, he took the letter from Margaret out of his pocket, read it again, and thought, *what happened to you?*

What happened, indeed. When she sent the letter a month ago, she must have been sleeping in this very bed. He looked at her spot, which appeared cruelly empty now, and considered it for a while.

*It's not right*, he thought, jumping to his feet. It's not that it looked different; it *was* different. It wasn't something another person would notice---for all intents and purposes, this was his house---but something deeper in him felt it. Felt that this wasn't the same place he had left. The warmth was missing, as if someone had taken his home with all its love an memories, and replaced it with a perfect replica. He needed to get out of here; whatever had happened here was still happening. This wasn't a safe place to be right now.

He left the house in a hurry. Left the town in a hurry. And he didn't look back until he was well outside its borders. And when he did look back, he wondered how he ever thought it was the same place he had grown up; it had an aura about it. The word occurred to him again: it was *wrong*.

*Margaret, where are you?* 

With that thought in mind, he set off into the woods. He didn't know where he was going, but he had to find out what happened here. He would find Margaret again, and when he did, he would finally apologize.
